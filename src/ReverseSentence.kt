fun reverseSentence(s:String):String
{
    var ans = String()
    var i =s.length-1
    var j =s.length
    while(j>=0 && i>=0) {
        if (s.get(i) == ' ') {
            ans += s.substring(i+1, j)+" "
            j=i
        }
        if(i==0)
        {
            ans+=s.substring(i,j+1)
        }
        i--
    }
    return ans

}

fun main()
{
    println("enter the sentence :")
    val s :String = readLine()!!.trim()
    val ans = reverseSentence(s)
    println("the reversed sentence is ")
    println(ans)

}