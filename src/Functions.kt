val numbers = arrayOf(1,-2,3,-4,5)

fun main() {
    println(numbers.filter {  it > 0 })

    println(numbers.filter { item -> item<0 })

    println("sum is = ${numbers.sum()}")

    val adding = { a: Int,b: Int -> a+b}
    val adding2:(Int,Int)->Int = {x,y->x+y}

    println(adding(9,10))
    println(adding2(9,90))
}