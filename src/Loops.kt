fun main() {
    //while loop
    var num = 1
    while (num<=10)
    {
        print("$num ")
        num++
    }
    println()
    println()

    //do while
    var num2 = 10
    do {
        print("$num2 ")
        num2--
    }while(num2>0)

    println()
    println()


    //for loop
    val arr = arrayOf("java","python","c","go","kotlin","c++","ruby","javascript")
    for(i in 0..arr.size-1)
    {
        print(arr[i]+" ")
    }

    println()
    println()

    for(i in arr.indices)
    {
        print(arr[i]+" ")
    }
    println()
    println()

    for(item in arr)
    {
        print("$item ")
    }

    println()
    println()

    for(i in 0..10 step 2)
    {
        print("$i ")
    }
    println()
    println()

    for(i in 100 downTo 0 step 10)
    {
        print("$i ")
    }
    println()
    println()

    for((i,v) in arr.withIndex())
    {
        println("at $i = $v")
    }
    println()
    println()
    //labeled break
    println("labelled break")
    outer@ for(i in 1..4)
    {
        inner@ for(j in 1..4)
        {
            if(i==4) {
                println("breaking outer loop")
                break@outer
            }
            if(j==3) {
                println("breaking inner loop")
                break@inner
            }

            println("i= $i and j= $j")
        }
    }

    println()
    println()

    //labelled continue
    println("labelled continue")

    there@ for(i in 10 downTo 5)
    {
        here@ for(j in 5 downTo 1)
        {
            if(j==2 || j==4)
            {
                continue@here
            }
            if (i<8)
            {
                continue@there
            }
            println("there= $i and here= $j")

        }
    }

}