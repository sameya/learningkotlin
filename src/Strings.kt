fun main(){

    var str = "Hello"
    println(str[0])
    println(str[1])
    println(str[2])
    println(str[3])
    println(str[4])


    var str2 = "Welcome"
    for(i in str2.indices){
        print(str2[i]+" ")
    }
    println()
    println(str.get(0))
    println("the length of the string $str is ${str.length}")


    var str3 = """My 
        |name 
        |is 
        |sameya 
    """.trimMargin()
    println(str3)


    val s1 = "hello"
    val s2 = "hello"
    val s3 = "byehello"

    println(s1==s2)
    println(s1===s2)
    println(s1==="hello")
    println(s1=="hello")
    println(s1===s3.substring(3))
    println(s1==s3.substring(3))
}