fun main() {
    val nums = arrayOf(10,20,30,40,50,60,90)
    val len = nums.size
    //print(len)
    print(binarySearch(nums, 40, 0, len-1))

}

fun binarySearch(arr : Array<Int>, key: Int , low:Int, high: Int):Boolean
{
    if(low<=high)
    {
        val mid = (low+high)/2
        if (arr[mid]== key)
        {
            return true
        }

        else if(arr[mid] > key)
        {
            return binarySearch(arr, key, low, mid-1)
        }

        else
        {
            return binarySearch(arr, key, mid+1, high)
        }
    }
    return false
}