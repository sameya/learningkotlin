class UserRegistration(userId: Int, userName: String, email: String, password: String, age: Int, gender: Char) {
    var userId: Int = userId
        private set

    var userName: String = userName

    var email: String = email

    var password: String = password

    var age: Int = age

    var gender: Char = gender

}

fun main() {
    var user1 = UserRegistration(1, "abc", "abc@gmail.com", "abc123", 25, 'M')
    println("user id = ${user1.userId}")
    //user1.userId = 101
    println("changed user id = ${user1.userId}")

}