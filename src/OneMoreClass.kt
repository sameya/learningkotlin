class Student
(
    var rollNo: Int,
    var name: String,
    var section: Char,
    var present: Boolean
)

class Quadrilateral
(
        var width:Int,
        var height:Int
)
{
    val isSquare: Boolean
    get() {
        return width==height
    }
    val isRectangle: Boolean
    get() {
        return width!=height
    }
}


fun main()
{
    var student1 = Student(1,"alex",'B', true)
    var stud2 = Student(2,"bob",'A',false)
    println("${student1.name} is present = ${student1.present}")
    println("${stud2.name} is present = ${stud2.present}")
    println("\n")

    var quad1 = Quadrilateral(10,10)
    println("quad1 is square ${quad1.isSquare}")
    println("quad1 is rectangle ${quad1.isRectangle}")
    println()
    var quad2 = Quadrilateral(12,7)
    println("quad2 is square ${quad2.isSquare}")
    println("quad2 is rectangle ${quad2.isRectangle}")
    println()

    var animal1 = Animal("mammal", "dog")
    println()

    var variable1 = Secondary(1)

}

class Animal constructor(type: String, species:String)
{
    init {
        println("the animal type = $type")
        println("the animal species = $species")
    }
}

open class Primary
{
    constructor(a:Int, b:Int, c:Int)
    {
        println("in Primary class")
    }
}
class Secondary : Primary {
    constructor(a:Int, b:Int):super(a, b, 10) {
        println("Constructor secondary")
    }

    constructor(a:Int) :this(a,10)
    {
        println("constructor2 in secondary")
    }

}