fun main() {
    print("Enter the string :" )

    val str = readLine()

    when (str) {
        "sameya" -> {
            println("hello $str, how have you been?")
        }

        "mom" -> {
            println("mom is sleeping")
        }

        else ->
        {
            println("default entry")
        }
    }
}
