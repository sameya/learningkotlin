fun main()
{
    println("Enter the num= ")
    val num: Int = readLine()!!.toInt()

    val ans = factorial(num)
    println(ans)

}

fun factorial(n: Int):Int
{
    if(n==0)
        return 1

    return n*factorial(n-1)
}
