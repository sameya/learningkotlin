fun main()
{
    println("enter the number = ")
    var num = readLine()!!.toInt()
    var reverseNum = 0
    while(num>0)
    {
        var rem = num%10
        //println("rem = $rem")
        num/=10
        //println("num = $num")
        reverseNum = reverseNum*10 + rem
        //println("reversenum = $reverseNum")
    }
    println("the reversed number is $reverseNum")
}