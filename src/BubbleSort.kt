fun main() {
    val nums = arrayOf(20,100,50,30,70,3,12)


    println("the unsorted array:")
    for( num in nums)
    {
        print("$num ")
    }
    println()

    bubbleSort(nums)
    println("the sorted array:")

    for( num in nums)
    {
        print("$num ")
    }

}

fun bubbleSort(arr: Array<Int>): Unit
{
    val len = arr.size -1

    for(i in 0..len-1)
    {
        for(j in i..len)
        {
            if(arr[i]>arr[j])
            {
                val temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            }
        }
    }
}