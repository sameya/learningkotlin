class Employee
{
    var name:String = ""
    var age:Int = 0
    var salary:Double = 0.0
    var id:String = ""

    fun insetValues(name: String, age: Int, salary: Double, id:String)
    {
        this.name = name
        this.age = age
        this.salary = salary
        this.id = id
        println("the name of the employee with id = $id is $name and the employee's age is $age with salary= $salary")
    }

    fun insertAge(age: Int)
    {
        this.age = age
    }

    fun insertName(name: String)
    {
        this.name = name
    }
    fun eat()
    {
        println("the employee $name is eating")
    }
}

fun main()
{
    var emp1 = Employee()
    emp1.insetValues(name = "jenny", age = 20, salary = 100.0, id = "E-100")
    emp1.eat()

    var emp2 = Employee()
    emp2.insertAge(30)
    emp2.insertName("alex")
    emp2.eat()
}