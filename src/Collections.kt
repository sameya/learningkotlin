fun main()
{
//    //Immutable
//    val list1 = listOf<Int>(10,20,30,40,50)
//    for(item in list1)
//        print("$item  ")
//
//    println()
//    println()
//    val set1 = setOf<String>("apple","banana","carrot","banana")
//    for(item in set1)
//        print("$item  ")
//
//    println()
//    println()
//    val map1 = mapOf<Int, String>(1 to "nursery",2 to "primary",3 to "secondary", 4 to "high school", 5 to "college")
//    for(item in map1)
//        println(item)
//
//    println()
//    println()
//    //Mutable
//    //var list2 = mutableListOf<String>("dad","mom","bro","sis")
//    var list2 = arrayListOf<String>("dad","mom","bro","sis")
//    list2.add("daddy")
//    list2.addAll(listOf("mommy","sister","brother"))
//    for(item in list2)
//        print("$item  ")
//
//    println()
//    println()
//
//    var set2 = mutableSetOf<Int>(10,20,20,20,30,50,90)
//    //var set2 = hashSetOf<>()<Int>(10,20,20,20,30,50,90)
//    println("set2 size is ${set2.size}")
//    set2.add(30)
//    set2.addAll(listOf(60,80,90,90,100))
//    println("set2 new size is ${set2.size}")
//    println()
//    println()
//
//    var map2 = hashMapOf((1 to "list"),(2 to "set"), 3 to "map")
//    //var map2 = mutableMapOf<>((1 to "list"),(2 to "set"), 3 to "map")
//    println("map2 size is ${map2.size}")
//    map2.put(4, "array")
//    println("value at key 3 is ${map2.get(3)}")
//
//    println()
//    println()


    //arraylist
    var al = ArrayList<Int>()
    al.add(10)
    al.add(20)
    for(i in 30..50 step 10)
        al.add(i)

    al.add(3,90)
    var al2 = ArrayList<Int>()
    al2.addAll(al)

    for(it in al2)
    {
        print("$it ")
    }
    println("\n")
    println("at index 4 ---->${al.get(4)}")
    al.set(4,70)
    println("now at index 4 ---->${al.get(4)}")

    println("index of 40 is ${al.indexOf(40)}")
    println("index of 50 is ${al.indexOf(50)}")

}