import java.util.*;

fun main() {

    // using Scanner

    println("enter an integer")
    val sc = Scanner(System.`in`)
    val num1: Int = sc.nextInt()
    println("the number is $num1")

    println("enter a float number")
    val num2: Float = sc.nextFloat()
    println("the number is $num2")

    println("enter some msg")
    val str:String = sc.next()
    println("the msg is : $str")


    //using readline

    println("enter a string")
    val msg:String = readLine()!!
    println("the entered string is : $msg")

    println("enter an integer number")
    val number:Int = readLine()!!.toInt()
    print("the entered numner is : $number")



}