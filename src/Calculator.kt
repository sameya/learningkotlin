import java.util.*

fun calc(a:Int, b:Int, op:Char) : Any{
    when(op){
        '+'-> return a+b
        '-'-> return a-b
        '*'-> return a*b
        '/'-> return a/b
        '%'-> return a%b
        else -> return println("not a valid operator")
    }
}

fun main()
{
    println("calculator")
    println("enter two numbers and operator ")
    val sc = Scanner(System.`in`)
    val num1 = sc.nextInt()
    val num2 = sc.nextInt()
    val op = sc.next().single()
    val ans = calc(num1,num2, op)
    println("ans = $ans")
}